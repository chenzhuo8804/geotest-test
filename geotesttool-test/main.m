//
//  main.m
//  geotesttool-test
//
//  Created by Zhuo Chen on 12/20/15.
//  Copyright © 2015 ZachChen. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *output = @"<?xml version=\"1.0\" ?>\n"
"<testsuite errors=\"1\" failures=\"5\" name=\"nose2-junit\" skips=\"1\" tests=\"25\" time=\"0.004\">\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_gen:1\" time=\"0.000141\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_gen:2\" time=\"0.000093\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_gen:3\" time=\"0.000086\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_gen:4\" time=\"0.000086\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_gen:5\" time=\"0.000087\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_gen_nose_style:1\" time=\"0.000085\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_gen_nose_style:2\" time=\"0.000090\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_gen_nose_style:3\" time=\"0.000085\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_gen_nose_style:4\" time=\"0.000087\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_gen_nose_style:5\" time=\"0.000086\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_params_func:1\" time=\"0.000093\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_params_func:2\" time=\"0.000098\">\n"
"    <failure message=\"test failure\">Traceback (most recent call last):\n"
"  File \"nose2/plugins/loader/parameters.py\", line 162, in func\n"
"    return obj(*argSet)\n"
"  File \"nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py\", line 64, in test_params_func\n"
"    assert a == 1\n"
"AssertionError\n"
"</failure>\n"
"  </testcase>\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_params_func_multi_arg:1\" time=\"0.000094\" />\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_params_func_multi_arg:2\" time=\"0.000089\">\n"
"    <failure message=\"test failure\">Traceback (most recent call last):\n"
"  File \"nose2/plugins/loader/parameters.py\", line 162, in func\n"
"    return obj(*argSet)\n"
"  File \"nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py\", line 69, in test_params_func_multi_arg\n"
"    assert a == b\n"
"AssertionError\n"
"</failure>\n"
"  </testcase>\n"
"  <testcase classname=\"pkg1.test.test_things\" name=\"test_params_func_multi_arg:3\" time=\"0.000096\" />\n"
"  <testcase classname=\"\" name=\"test_fixt\" time=\"0.000091\" />\n"
"  <testcase classname=\"\" name=\"test_func\" time=\"0.000084\" />\n"
"  <testcase classname=\"pkg1.test.test_things.SomeTests\" name=\"test_failed\" time=\"0.000113\">\n"
"    <failure message=\"test failure\">Traceback (most recent call last):\n"
"  File \"nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py\", line 17, in test_failed\n"
"    assert False, \"I failed\"\n"
"AssertionError: I failed\n"
"</failure>\n"
"  </testcase>\n"
"  <testcase classname=\"pkg1.test.test_things.SomeTests\" name=\"test_ok\" time=\"0.000093\" />\n"
"  <testcase classname=\"pkg1.test.test_things.SomeTests\" name=\"test_params_method:1\" time=\"0.000099\" />\n"
"  <testcase classname=\"pkg1.test.test_things.SomeTests\" name=\"test_params_method:2\" time=\"0.000101\">\n"
"    <failure message=\"test failure\">Traceback (most recent call last):\n"
"  File \"nose2/plugins/loader/parameters.py\", line 144, in _method\n"
"    return method(self, *argSet)\n"
"  File \"nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py\", line 29, in test_params_method\n"
"    self.assertEqual(a, 1)\n"
"AssertionError: 2 != 1\n"
"</failure>\n"
"  </testcase>\n"
"  <testcase classname=\"pkg1.test.test_things.SomeTests\" name=\"test_skippy\" time=\"0.000104\">\n"
"    <skipped />\n"
"  </testcase>\n"
"  <testcase classname=\"pkg1.test.test_things.SomeTests\" name=\"test_typeerr\" time=\"0.000096\">\n"
"    <error message=\"test failure\">Traceback (most recent call last):\n"
"  File \"nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py\", line 13, in test_typeerr\n"
"    raise TypeError(\"oops\")\n"
"TypeError: oops\n"
"</error>\n"
"  </testcase>\n"
"  <testcase classname=\"pkg1.test.test_things.SomeTests\" name=\"test_gen_method:1\" time=\"0.000094\" />\n"
"  <testcase classname=\"pkg1.test.test_things.SomeTests\" name=\"test_gen_method:2\" time=\"0.000090\">\n"
"    <failure message=\"test failure\">Traceback (most recent call last):\n"
"  File \"nose2/plugins/loader/generators.py\", line 145, in method\n"
"    return func(*args)\n"
"  File \"nose2/tests/functional/support/scenario/tests_in_package/pkg1/test/test_things.py\", line 24, in check\n"
"    assert x == 1\n"
"AssertionError\n"
"</failure>\n"
"  </testcase>\n"
"</testsuite>";

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"%@", output);
    }
    return 0;
}
